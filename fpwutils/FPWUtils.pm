#                                                         -*- Perl -*-
# Copyright (c) 1999, 2000  Motoyuki Kasahara
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#

package FreePWING::FPWUtils::FPWUtils;

require 5.005;
use strict;
use integer;

use vars qw(@ISA
            @EXPORT
            @EXPORT_OK
	    $work_directory
	    $text_file_name
	    $text_ref_file_name
	    $text_tag_file_name
	    $menu_file_name
	    $menu_ref_file_name
	    $menu_tag_file_name
	    $copyright_file_name
	    $copyright_ref_file_name
	    $copyright_tag_file_name
	    $heading_file_name
	    $word_file_name
	    $endword_file_name
	    $keyword_file_name
	    $sort_file_name
	    $endsort_file_name
	    $keysort_file_name
	    $index_file_name
	    $endindex_file_name
	    $keyindex_file_name
	    $index_ref_file_name
	    $endindex_ref_file_name
	    $keyindex_ref_file_name
	    $control_file_name
	    $control_ref_file_name
	    $color_graphic_file_name
	    $color_graphic_tag_file_name
	    $sound_file_name
	    $sound_tag_file_name
	    $sound_fmt_file_name
	    $honmon_file_name
	    $half_char_name_file_name
	    $full_char_name_file_name
	    $half_char_bitmap_16_file_name
	    $half_char_bitmap_24_file_name
	    $half_char_bitmap_30_file_name
	    $half_char_bitmap_48_file_name
	    $full_char_bitmap_16_file_name
	    $full_char_bitmap_24_file_name
	    $full_char_bitmap_30_file_name
	    $full_char_bitmap_48_file_name
	    $sort_command);

@ISA = qw(Exporter);
@EXPORT = qw($work_directory
	     $text_file_name
	     $text_ref_file_name
	     $text_tag_file_name
	     $menu_file_name
	     $menu_ref_file_name
	     $menu_tag_file_name
	     $copyright_file_name
	     $copyright_ref_file_name
	     $copyright_tag_file_name
	     $heading_file_name
	     $word_file_name
	     $endword_file_name
	     $keyword_file_name
	     $sort_file_name
	     $endsort_file_name
	     $keysort_file_name
	     $index_file_name
	     $endindex_file_name
	     $keyindex_file_name
	     $index_ref_file_name
	     $endindex_ref_file_name
	     $keyindex_ref_file_name
	     $control_file_name
	     $control_ref_file_name
	     $color_graphic_file_name
	     $color_graphic_tag_file_name
	     $sound_file_name
	     $sound_tag_file_name
	     $sound_fmt_file_name
	     $honmon_file_name
	     $half_char_name_file_name
	     $full_char_name_file_name
	     $half_char_bitmap_16_file_name
	     $half_char_bitmap_24_file_name
	     $half_char_bitmap_30_file_name
	     $half_char_bitmap_48_file_name
	     $full_char_bitmap_16_file_name
	     $full_char_bitmap_24_file_name
	     $full_char_bitmap_30_file_name
	     $full_char_bitmap_48_file_name
	     $sort_command
	     initialize_fpwutils
	     finalize_fpwutils);

# 作業ファイルを書き込むディレクトリ
$work_directory = '.';

#
# 設定用の変数の初期化を行う。
#
sub initialize_fpwutils {
    # 本文ファイル名
    $text_file_name = "$work_directory/text";

    # 本文の参照情報ファイル名
    $text_ref_file_name = "$work_directory/textref";
    
    # 本文のタグファイル名
    $text_tag_file_name = "$work_directory/texttag";
    
    # メニューファイル名
    $menu_file_name = "$work_directory/menu";
    
    # メニューの参照情報ファイル名
    $menu_ref_file_name = "$work_directory/menuref";
    
    # メニューのタグファイル名
    $menu_tag_file_name = "$work_directory/menutag";
    
    # 著作権表示ファイル名
    $copyright_file_name = "$work_directory/copy";
    
    # 著作権表示の参照情報ファイル名
    $copyright_ref_file_name = "$work_directory/copyref";
    
    # 著作権表示のタグファイル名
    $copyright_tag_file_name = "$work_directory/copytag";
    
    # 見出しファイル名
    $heading_file_name = "$work_directory/head";
    
    # 前方一致用の未整列単語一覧ファイル名
    $word_file_name = "$work_directory/word";
    
    # 後方一致用の未整列単語一覧ファイル名
    $endword_file_name = "$work_directory/eword";
    
    # 条件検索用の未整列単語一覧ファイル名
    $keyword_file_name = "$work_directory/kword";
    
    # 前方一致用の整列済み単語一覧ファイル名
    $sort_file_name = "$work_directory/sort";
    
    # 後方一致用の整列済み単語一覧ファイル名
    $endsort_file_name = "$work_directory/esort";
    
    # 条件検索用の整列済み単語一覧ファイル名
    $keysort_file_name = "$work_directory/ksort";
    
    # 前方一致インデックスの参照情報ファイル名
    $index_file_name = "$work_directory/idx";
    
    # 後方一致インデックスファイル名
    $endindex_file_name = "$work_directory/eidx";
    
    # 条件検索インデックスファイル名
    $keyindex_file_name = "$work_directory/kidx";
    
    # 前方一致インデックスファイル名
    $index_ref_file_name = "$work_directory/idxref";
    
    # 後方一致インデックスの参照情報ファイル名
    $endindex_ref_file_name = "$work_directory/eidxref";
    
    # 条件検索インデックスの参照情報ファイル名
    $keyindex_ref_file_name = "$work_directory/kidxref";
    
    # 書籍管理情報ファイル名
    $control_file_name = "$work_directory/ctrl";
    
    # 書籍管理情報の参照情報ファイル名
    $control_ref_file_name = "$work_directory/ctrlref";
    
    # カラー図版ファイル名
    $color_graphic_file_name = "$work_directory/cgr";

    # カラー図版タグファイル名
    $color_graphic_tag_file_name = "$work_directory/cgrtag";

    # 音声ファイル名
    $sound_file_name = "$work_directory/snd";

    # 音声タグファイル名
    $sound_tag_file_name = "$work_directory/sndtag";

    # 音声形式ファイル名 (satomii)
    $sound_fmt_file_name = "$work_directory/sndfmt";

    # HONMON ファイル名
    $honmon_file_name = "honmon";
    
    # 半角外字名前ファイル
    $half_char_name_file_name = "$work_directory/halfchar";
    
    # 全角外字名前ファイル
    $full_char_name_file_name = "$work_directory/fullchar";
    
    # 半角外字ビットマップファイル名
    $half_char_bitmap_16_file_name = "gai16h";
    $half_char_bitmap_24_file_name = "gai24h";
    $half_char_bitmap_30_file_name = "gai30h";
    $half_char_bitmap_48_file_name = "gai48h";
    
    # 全角外字ビットマップファイル名
    $full_char_bitmap_16_file_name = "gai16f";
    $full_char_bitmap_24_file_name = "gai24f";
    $full_char_bitmap_30_file_name = "gai30f";
    $full_char_bitmap_48_file_name = "gai48f";
}

#
# 設定用の変数の後始末を行う。
#
sub finalize_fpwutils {
}

1;
