#                                                         -*- Perl -*-
# Copyright (c) 2000  Motoyuki Kasahara
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#

#
# 条件検索クラス
#
package FreePWING::FPWUtils::KeyWord;

require 5.005;
use English;
use FreePWING::KeyWord;
use FreePWING::FPWUtils::FPWUtils;
use strict;
use integer;

use vars qw(@ISA
	    @EXPORT
	    @EXPORT_OK);

@ISA = qw(FreePWING::KeyWord);

#
# 書式:
#	new()
# メソッドの区分:
# 	public クラスメソッド。
# 説明:
# 	新しいオブジェクトを作る。
# 戻り値:
# 	作成したオブジェクトへのリファレンスを返す。
#
sub new {
    my $type = shift;
    return $type->SUPER::new();
}

#
# 書式:
#	open()
# メソッドの区分:
# 	public インスタンスメソッド。
# 説明:
# 	書き込み用の見出しファイルを開く。
# 戻り値:
#	成功すれば 1 を返す。失敗すれば 0 を返す。
#
sub open {
    my $self = shift;
    
    return $self->SUPER::open($keyword_file_name);
}

#
# 書式:
#	add_entry(word)
#           word
#		単語
#           heading_position
#               見出しの位置
#           text_position
#               本文の位置
# メソッドの区分:
# 	public インスタンスメソッド。
# 説明:
# 	単語ファイルに単語を一つ追加する。
# 戻り値:
#	成功すれば 1 を返す。失敗すれば 0 を返す。
#
sub add_entry {
    my $self = shift;
    my ($word, $heading_position, $text_position) = @ARG;

    return $self->SUPER::add_entry($word,
				   $heading_position, $heading_file_name,
				   $text_position, $text_file_name);
}

1;
