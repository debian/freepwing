#! @PERL@
#                                                         -*- Perl -*-
# Copyright (c) 1999, 2000  Motoyuki Kasahara
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#

require 5.005;

use English;
use FreePWING::Control;
use FreePWING::FPWUtils::FPWUtils;
use Getopt::Long;

#
# コマンド行を解析する。
#
if (!GetOptions('workdir=s' => \$work_directory)) {
    exit 1;
}

#
# fpwutils を初期化する。
#
initialize_fpwutils();

#
# これから出力するファイルがすでにあれば、削除する。
#
unlink($control_file_name);
unlink($control_ref_file_name);

#
# 書籍管理情報ファイルを開く。
#
$control = FreePWING::Control->new();
if (!$control->open($control_file_name, $control_ref_file_name)) {
    die "$PROGRAM_NAME: " . $control->error_message() . "\n";
}

#
# 本文データエントリを追加する。
#
if (-f $text_file_name
    && !$control->add_text_entry($text_file_name)) {
    die "$PROGRAM_NAME: " . $control->error_message() . "\n";
}

#
# 見出しエントリを追加する。
#
if (-f $heading_file_name
    && !$control->add_heading_entry($heading_file_name)) {
    die "$PROGRAM_NAME: " . $control->error_message() . "\n";
}

#
# 前方一致インデックスエントリを追加する。
#
$i = 0;
@file_names = ();
while (-f "$index_file_name$i") {
    unshift(@file_names, "$index_file_name$i");
    $i++;
}
if (0 < $i && !$control->add_index_entry(@file_names)) {
    die "$PROGRAM_NAME: " . $control->error_message() . "\n";
}

#
# 後方一致インデックスエントリを追加する。
#
$i = 0;
@file_names = ();
while (-f "$endindex_file_name$i") {
    unshift(@file_names, "$endindex_file_name$i");
    $i++;
}
if (0 < $i && !$control->add_endindex_entry(@file_names)) {
    die "$PROGRAM_NAME: " . $control->error_message() . "\n";
}

#
# 条件検索インデックスエントリを追加する。
#
$i = 0;
@file_names = ();
while (-f "$keyindex_file_name$i") {
    unshift(@file_names, "$keyindex_file_name$i");
    $i++;
}
if (0 < $i && !$control->add_keyindex_entry(@file_names)) {
    die "$PROGRAM_NAME: " . $control->error_message() . "\n";
}

#
# メニュー表示用データエントリを追加する。
#
if (-f $menu_file_name
    && !$control->add_menu_entry($menu_file_name)) {
    die "$PROGRAM_NAME: " . $control->error_message() . "\n";
}

#
# 著作権表示データエントリを追加する。
#
if (-f $copyright_file_name
    && !$control->add_copyright_entry($copyright_file_name)) {
    die "$PROGRAM_NAME: " . $control->error_message() . "\n";
}

#
# カラー図版エントリを追加する。
#
if (-f $color_graphic_file_name
    && !$control->add_color_graphic_entry($color_graphic_file_name)) {
    die "$PROGRAM_NAME: " . $control->error_message() . "\n";
}

#
# 音声エントリを追加する。
#
if (-f $sound_file_name
    && !$control->add_sound_entry($sound_file_name)) {
    die "$PROGRAM_NAME: " . $control->error_message() . "\n";
}

#
# 書籍管理情報ファイルを閉じる。
#
if (!$control->close()) {
    die "$PROGRAM_NAME: " . $control->error_message() . "\n";
}

#
# fpwutils の後始末をする。
#
finalize_fpwutils();

exit 0;
