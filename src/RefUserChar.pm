#                                                         -*- Perl -*-
# Copyright (c) 2000  Motoyuki Kasahara
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#

#
# 外字名前ファイルを参照する。
#
package FreePWING::RefUserChar;

require 5.005;
require Exporter;
use English;
use FileHandle;
use FreePWING::Reference;
use strict;
use integer;

use vars qw(@ISA
	    @EXPORT
	    @EXPORT_OK);

@ISA = qw(Exporter);

#
# 書式:
#	new()
# メソッドの区分:
# 	public クラスメソッド。
# 説明:
# 	新しいオブジェクトを作る。
# 戻り値:
# 	作成したオブジェクトへのリファレンスを返す。
#
sub new {
    my $type = shift;
    my $new = {
	# 外字テーブル
	'characters' => {},

	# エラーメッセージ
	'error_message' => '',
    };
    return bless($new, $type);
}

#
# 書式:
#	add_characters_in_file(file_name)
#           file_name
#		読み込む外字名前ファイルの名前
# メソッドの区分:
# 	public インスタンスメソッド。
# 説明:
# 	外字名前ファイルに登録されている名前を読み込む。
# 戻り値:
#	成功すれば 1 を返す。失敗すれば 0 を返す。
#
sub add_characters_in_file {
    my $self = shift;
    my ($file_name) = @ARG;

    #
    # 外字名前ファイルを開く。
    #
    my $handle = FileHandle->new();
    if (!$handle->open($file_name, 'r')) {
	$self->{'error_message'} = 
	    "failed to open the file, $ERRNO: $file_name";
	return 0;
    }

    #
    # 外字名前ファイルの各行を読み込む。
    #
    my $line;
    my @line_fields;
    for (;;) {
	$line = $handle->getline();
	if (!defined($line)) {
	    last;
	}
	chomp $line;
	@line_fields = split(/\t/, $line);
	if (defined($self->{'characters'}->{$line_fields[0]})) {
	    $self->{'error_message'} = 
		"redefined character name, $line_fields[0]: " 
		    . "line $NR, $file_name";
	    $handle->close();
	    return 0;
	}
	$self->{'characters'}->{$line_fields[0]} = hex($line_fields[1]);
    }

    #
    # 外字名前ファイルを閉じる。
    #
    $handle->close();
    
    return 1;
}

######################################################################
# <インスタンス変数の値を返すメソッド群>
#
# 書式:
#	インスタンス変数名()
# メソッドの区分:
# 	public インスタンスメソッド。
# 戻り値:
#	インスタンス変数の値を返す。
#
sub character {
    my $self = shift;
    my ($name) = @ARG;
    return $self->{'characters'}->{$name};
}

sub character_count {
    my $self = shift;
    return scalar(%{$self->{'characters'}}) + 0;
}

sub error_message {
    my $self = shift;
    return $self->{'error_message'};
}

1;

