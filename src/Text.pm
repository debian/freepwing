#                                                         -*- Perl -*-
# Copyright (c) 1999, 2000  Motoyuki Kasahara
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#

#
# 本文を収めたファイルを生成するためのクラス。
#
package FreePWING::Text;

require 5.005;
use English;
use FreePWING::BaseText;
use strict;
use integer;

use vars qw(@ISA
	    @EXPORT
	    @EXPORT_OK);

@ISA = qw(FreePWING::BaseText);

#
# 書式:
#	new()
# メソッドの区分:
# 	public クラスメソッド。
# 説明:
# 	新しいオブジェクトを作る。
# 戻り値:
# 	作成したオブジェクトへのリファレンスを返す。
#
sub new {
    my $type = shift;
    return $type->SUPER::new();
}

#
# 書式:
#	open(file_name, reference_file_name, tag_file_name)
#           file_name
#		本文ファイルの名前。
#           reference_file_name
#		参照情報ファイルの名前。
#           tag_file_name
#		タグファイルの名前。
# メソッドの区分:
# 	public インスタンスメソッド。
# 説明:
# 	書き込み用の本文ファイルを開く。
# 戻り値:
#	成功すれば 1 を返す。失敗すれば 0 を返す。
#
sub open {
    my $self = shift;
    my ($file_name, $reference_file_name, $tag_file_name) = @ARG;
    
    return $self->SUPER::open($file_name, $reference_file_name,
			      $tag_file_name);
}

#
# 書式:
#	add_entry_start()
# メソッドの区分:
# 	protected インスタンスメソッド。
# 説明:
# 	新たなエントリの開始を示す制御記述子を追加する。
# 戻り値:
#	成功すれば 1 を返す。失敗すれば 0 を返す。
#
sub add_entry_start {
    my $self = shift;

    return $self->write_data(pack('nn', 0x1f09, 0x0001));
}

1;

