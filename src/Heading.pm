#                                                         -*- Perl -*-
# Copyright (c) 1999, 2000  Motoyuki Kasahara
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#

#
# 見出しを収めたファイルを生成するためのクラス。
#
package FreePWING::Heading;

require 5.005;
use English;
use FreePWING::BaseText;
use strict;
use integer;

use vars qw(@ISA
	    @EXPORT
	    @EXPORT_OK);

@ISA = qw(FreePWING::BaseText);

#
# 書式:
#	new()
# メソッドの区分:
# 	public クラスメソッド。
# 説明:
# 	新しいオブジェクトを作る。
# 戻り値:
# 	作成したオブジェクトへのリファレンスを返す。
#
sub new {
    my $type = shift;
    return $type->SUPER::new();
}

#
# 書式:
#	open(file_name)
#           file_name
#		見出しファイルの名前。
# メソッドの区分:
# 	public インスタンスメソッド。
# 説明:
# 	書き込み用の見出しファイルを開く。
# 戻り値:
#	成功すれば 1 を返す。失敗すれば 0 を返す。
#
sub open {
    my $self = shift;
    my ($file_name) = @ARG;
    
    return $self->SUPER::open($file_name);
}

#
# 書式:
#	add_entry_end()
# メソッドの区分:
# 	protected インスタンスメソッド。
# 説明:
# 	現在のエントリの終端を示す制御記述子を追加する。
# 戻り値:
#	成功すれば 1 を返す。失敗すれば 0 を返す。
#
sub add_entry_end {
    my $self = shift;
    
    if (!$self->SUPER::add_entry_end()) {
	return 0;
    }
    if (!$self->write_data(pack('n', 0x1f0a))) {
	return 0;
    }
    return 1;
}

#
# FreePWING::BaseText で定義されている制御記述子追加メソッドをいくつ
# か取り消す。
#
undef &new_context;
undef &add_tag;
undef &add_keyword_start;
undef &add_keyword_end;
undef &add_reference_start;
undef &add_reference_end;
undef &add_newline;
undef &add_indent_level;

1;

