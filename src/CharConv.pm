#                                                         -*- Perl -*-
# Copyright (c) 1999, 2000  Motoyuki Kasahara
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#

#
# ʸ�����Ѵ��ơ��֥��Ѥ������ export ���뤿��Υѥå�������
#
package FreePWING::CharConv;

require 5.005;
require Exporter;
use strict;
use integer;

use vars qw(@ISA
	    @EXPORT
	    @EXPORT_OK
	    $ascii_to_jisx0208_table
	    $jisx0201_to_jisx0208_table);

@ISA = qw(Exporter);
@EXPORT = qw($ascii_to_jisx0208_table
	     $jisx0201_to_jisx0208_table);

#
# ASCII (2/0 �� 7/14) �� JIS X 0208 ���Ѵ�
#
$ascii_to_jisx0208_table
    = [#      ��    ��    ��    ��    ��    ��    ��
       '!!', '!*', '!I', '!t', '!p', '!s', '!u', '!G',

       #��     ��   ��    ��    ��    ��    ��    ��
       '!J', '!K', '!v', "!\\",'!$', '!]', '!%', '!?',

       #��    ��    ��    ��    ��    ��    ��    ��
       '#0', '#1', '#2', '#3', '#4', '#5', '#6', '#7',

       #��    ��    ��    ��    ��    ��    ��    ��
       '#8', '#9', "!'", '!(', '!c', '!a', '!d', '!)',

       #��    ��    ��    ��    ��    ��    ��    ��
       '!w', '#A', '#B', '#C', '#D', '#E', '#F', '#G',

       #��    ��    ��    ��    ��    ��    ��    ��
       '#H', '#I', '#J', '#K', '#L', '#M', '#N', '#O',

       #��    ��    ��    ��    ��    ��    ��    ��
       '#P', '#Q', '#R', '#S', '#T', '#U', '#V', '#W',

       #��    ��    ��    ��    ��    ��    ��    ��
       '#X', '#Y', '#Z', '!N', '!@', '!O', '!0', '!2',

       #��    ��    ��    ��    ��    ��    ��    ��
       '!F', '#a', '#b', '#c', '#d', '#e', '#f', '#g',

       #��    ��    ��    ��    ��    ��    ��    ��
       '#h', '#i', '#j', '#k', '#l', '#m', '#n', '#o',

       #��    ��    ��    ��    ��    ��    ��    ��
       '#p', '#q', '#r', '#s', '#t', '#u', '#v', '#w',

       #��    ��    ��    ��    ��    ��    ��
       '#x', '#y', '#z', '!P', '!C', '!Q', '!1',
];

#
# JIS X 0201 ���� (2/15 �� 5/15) �� JIS X 0208 ���Ѵ�
#
$jisx0201_to_jisx0208_table
    = [#      ��    ��    ��    ��    ��    ��    ��   
       '', '!#', '!V', '!W', '!"', '!&', '%r', '%!',

       #��    ��    ��    ��    ��    ��    ��    ��   
       '%#', '%%', "\%'",'%)', '%c', '%e', '%g', '%C',

       #��    ��    ��    ��    ��    ��    ��    ��   
       '!<', '%"', '%$', '%&', '%(', '%*', '%+', '%-',

       #��    ��    ��    ��    ��    ��    ��    ��   
       '%/', '%1', '%3', '%5', '%7', '%9', '%;', '%=',

       #��    ��    ��    ��    ��    ��    ��    ��   
       '%?', '%A', '%D', '%F', '%H', '%J', '%K', '%L',

       #��    ��    ��    ��    ��    ��    ��    ��   
       '%M', '%N', '%O', '%R', '%U', '%X', '%[', '%^',

       #��    ��    ��    ��    ��    ��    ��    ��   
       '%_', '%`', '%a', '%b', '%d', '%f', '%h', '%i',

       #��    ��    ��    ��    ��    ��    ��    ��   
       '%j', '%k', '%l', '%m', '%o', '%s', '!+', '!,',
];

1;
