freepwing (1.6.1-1) unstable; urgency=medium

  * QA upload.

  [ Boyuan Yang ]
  * debian/fpwmake.1: Use ISO date format.
  * debian/watch: Force FTP PASV mode for better compatibility.
  * d/control:
    - Add Salsa repository in VCS-Git and VCS-Browser
    - Bump Standards-Version to 4.7.0, no changes needed.

  [ Leandro Cunha ]
  * New upstream version.
  * d/copyright: Add myself.
  * Add d/salsa-ci.yml for Salsa-CI.

 -- Boyuan Yang <byang@debian.org>  Fri, 19 Apr 2024 22:05:14 -0400

freepwing (1.5-4) unstable; urgency=medium

  * QA upload.

  * Added d/gbp.conf to enforce the use of pristine-tar.
  * Updated vcs in d/control to Salsa.
  * Updated Standards-Version from 4.6.2 to 4.7.0.

 -- Petter Reinholdtsen <pere@debian.org>  Mon, 15 Apr 2024 19:11:06 +0200

freepwing (1.5-3) unstable; urgency=medium

  * QA upload.
  * Ran wrap-and-sort.
  * Using new DH level format. Consequently:
      - debian/compat: removed.
      - debian/control: changed from 'debhelper' to 'debhelper-compat' in
        Build-Depends field and bumped level to 13.
  * debian/control:
      - Added 'Rules-Requires-Root: no' to source stanza.
      - Bumped Standards-Version to 4.6.2.
  * debian/copyright:
      - Added missing licensing information.
      - Updated to 1.0 format.
  * debian/fpwmake.1: converted from EUC-JIS X 0213 to UTF-8.
  * debian/rules: removed useless comment.
  * debian/watch: created.

 -- David da Silva Polverari <polverari@debian.org>  Sat, 14 Oct 2023 01:50:09 +0000

freepwing (1.5-2) unstable; urgency=medium

  * QA upload.
  * Set Maintainer to Debian QA Group.  (See: #840887)
  * Switch to debhelper compat level 10, thanks to Logan Rosen.
    (Closes: #817461)
  * Switch to minimal dh rules.
  * Switch to source format 3.0 (quilt).

 -- Andreas Beckmann <anbe@debian.org>  Thu, 05 Jan 2017 12:35:02 +0100

freepwing (1.5-1) unstable; urgency=low

  * New upstream release
  * Bumped to Standards-Version: 3.8.0.

 -- Masayuki Hatta (mhatta) <mhatta@debian.org>  Thu, 17 Jul 2008 08:34:27 +0900

freepwing (1.4.4-3) unstable; urgency=low

  * Bumped to Standards-Version: 3.7.3.
  * Fixed various lintian warnings.

 -- Masayuki Hatta (mhatta) <mhatta@debian.org>  Fri, 07 Dec 2007 05:37:13 +0900

freepwing (1.4.4-2) unstable; urgency=low

  * Oops, I really fixed a typo this time.

 -- Masayuki Hatta (mhatta) <mhatta@debian.org>  Sat, 14 Oct 2006 12:47:19 +0900

freepwing (1.4.4-1) unstable; urgency=low

  * New upstream release - closes: #356376
  * Fixed a typo in description - closes: #363459
  * Bumped to Standards-Version: 3.7.2.

 -- Masayuki Hatta (mhatta) <mhatta@debian.org>  Sat, 14 Oct 2006 12:40:39 +0900

freepwing (1.4.3-1) unstable; urgency=low

  * New maintainer.
  * New upstream release - closes: #144467

 -- Masayuki Hatta (mhatta) <mhatta@debian.org>  Sat,  7 Feb 2004 23:12:29 +0900

freepwing (1.3.1-1) unstable; urgency=low

  * New upstream release (closes: #93300)
  * Rebuilt to update perl dependencies (closes: #80726)
  * Added doc-base.

 -- Masato Taruishi <taru@debian.org>  Mon,  9 Apr 2001 14:26:15 +0900

freepwing (1.3-1) unstable; urgency=low

  * New upstream release

 -- Masato Taruishi <taru@debian.org>  Tue, 24 Oct 2000 19:39:22 +0900

freepwing (1.2.1-1) unstable; urgency=low

  * Initial Release.

 -- Masato Taruishi <taru@debian.org>  Sun, 15 Oct 2000 23:19:57 +0900
